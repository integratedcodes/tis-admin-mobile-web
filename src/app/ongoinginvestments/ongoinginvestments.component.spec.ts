import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OngoinginvestmentsComponent } from './ongoinginvestments.component';

describe('OngoinginvestmentsComponent', () => {
  let component: OngoinginvestmentsComponent;
  let fixture: ComponentFixture<OngoinginvestmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OngoinginvestmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OngoinginvestmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
