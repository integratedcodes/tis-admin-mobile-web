import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TapheaderRoutingModule } from './tapheader-routing.module';
import { TapheaderComponent } from './tapheader.component';


@NgModule({
  declarations: [TapheaderComponent],
  imports: [
    CommonModule,
    TapheaderRoutingModule
  ]
})
export class TapheaderModule { }
