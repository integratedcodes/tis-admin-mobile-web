import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DueinvestmentsComponent } from './dueinvestments.component';

describe('DueinvestmentsComponent', () => {
  let component: DueinvestmentsComponent;
  let fixture: ComponentFixture<DueinvestmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DueinvestmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DueinvestmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
