import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DueinvestmentsRoutingModule } from './dueinvestments-routing.module';
import { DueinvestmentsComponent } from './dueinvestments.component';


@NgModule({
  declarations: [DueinvestmentsComponent],
  imports: [
    CommonModule,
    DueinvestmentsRoutingModule
  ]
})
export class DueinvestmentsModule { }
