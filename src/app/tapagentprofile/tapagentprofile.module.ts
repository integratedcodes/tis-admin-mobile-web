import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TapagentprofileRoutingModule } from './tapagentprofile-routing.module';
import { TapagentprofileComponent } from './tapagentprofile.component';


@NgModule({
  declarations: [TapagentprofileComponent],
  imports: [
    CommonModule,
    TapagentprofileRoutingModule
  ]
})
export class TapagentprofileModule { }
