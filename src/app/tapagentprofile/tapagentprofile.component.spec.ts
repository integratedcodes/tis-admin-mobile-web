import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TapagentprofileComponent } from './tapagentprofile.component';

describe('TapagentprofileComponent', () => {
  let component: TapagentprofileComponent;
  let fixture: ComponentFixture<TapagentprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TapagentprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TapagentprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
