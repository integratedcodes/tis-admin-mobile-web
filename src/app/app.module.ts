import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { ActivitiesModuleModule } from './activities/activities-module.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardModuleModule } from './dashboard/dashboard-module.module';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ActivitiesModuleModule,
    DashboardModuleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
