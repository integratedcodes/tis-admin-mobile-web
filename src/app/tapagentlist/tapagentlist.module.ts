import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TapagentlistRoutingModule } from './tapagentlist-routing.module';
import { TapagentlistComponent } from './tapagentlist.component';


@NgModule({
  declarations: [TapagentlistComponent],
  imports: [
    CommonModule,
    TapagentlistRoutingModule
  ]
})
export class TapagentlistModule { }
