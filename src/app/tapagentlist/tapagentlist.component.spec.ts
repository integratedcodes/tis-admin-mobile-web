import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TapagentlistComponent } from './tapagentlist.component';

describe('TapagentlistComponent', () => {
  let component: TapagentlistComponent;
  let fixture: ComponentFixture<TapagentlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TapagentlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TapagentlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
