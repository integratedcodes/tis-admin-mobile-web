import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedinvestmentsdetailsComponent } from './completedinvestmentsdetails.component';

describe('CompletedinvestmentsdetailsComponent', () => {
  let component: CompletedinvestmentsdetailsComponent;
  let fixture: ComponentFixture<CompletedinvestmentsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedinvestmentsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedinvestmentsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
