import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompletedinvestmentsdetailsRoutingModule } from './completedinvestmentsdetails-routing.module';
import { CompletedinvestmentsdetailsComponent } from './completedinvestmentsdetails.component';


@NgModule({
  declarations: [CompletedinvestmentsdetailsComponent],
  imports: [
    CommonModule,
    CompletedinvestmentsdetailsRoutingModule
  ]
})
export class CompletedinvestmentsdetailsModule { }
