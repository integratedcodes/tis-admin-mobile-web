import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnapprovedinvestmentsComponent } from './unapprovedinvestments.component';



@NgModule({
  declarations: [UnapprovedinvestmentsComponent],
  imports: [
    CommonModule
  ]
})
export class UnapprovedinvestmentsModule { }
