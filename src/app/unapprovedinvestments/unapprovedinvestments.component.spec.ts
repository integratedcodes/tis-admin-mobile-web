import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnapprovedinvestmentsComponent } from './unapprovedinvestments.component';

describe('UnapprovedinvestmentsComponent', () => {
  let component: UnapprovedinvestmentsComponent;
  let fixture: ComponentFixture<UnapprovedinvestmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnapprovedinvestmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnapprovedinvestmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
