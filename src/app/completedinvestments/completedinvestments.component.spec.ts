import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedinvestmentsComponent } from './completedinvestments.component';

describe('CompletedinvestmentsComponent', () => {
  let component: CompletedinvestmentsComponent;
  let fixture: ComponentFixture<CompletedinvestmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedinvestmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedinvestmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
