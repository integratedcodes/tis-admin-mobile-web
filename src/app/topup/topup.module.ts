import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopupRoutingModule } from './topup-routing.module';
import { TopupComponent } from './topup.component';


@NgModule({
  declarations: [TopupComponent],
  imports: [
    CommonModule,
    TopupRoutingModule
  ]
})
export class TopupModule { }
