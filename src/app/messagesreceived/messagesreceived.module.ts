import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesreceivedComponent } from './messagesreceived.component';



@NgModule({
  declarations: [MessagesreceivedComponent],
  imports: [
    CommonModule
  ]
})
export class MessagesreceivedModule { }
