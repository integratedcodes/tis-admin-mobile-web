import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesreceivedComponent } from './messagesreceived.component';

describe('MessagesreceivedComponent', () => {
  let component: MessagesreceivedComponent;
  let fixture: ComponentFixture<MessagesreceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesreceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesreceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
