import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestmentdetailactiveRoutingModule } from './investmentdetailactive-routing.module';
import { InvestmentlistdetailactiveComponent } from '../investmentlistdetailactive/investmentlistdetailactive.component';


@NgModule({
  declarations: [InvestmentlistdetailactiveComponent],
  imports: [
    CommonModule,
    InvestmentdetailactiveRoutingModule
  ]
})
export class InvestmentdetailactiveModule { }
