import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivitiesModuleRoutingModule } from './activities-module-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ActivitiesModuleRoutingModule
  ]
})
export class ActivitiesModuleModule { }
