import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActiveinvestmentsRoutingModule } from './activeinvestments-routing.module';
import { ActiveinvestmentsComponent } from './activeinvestments.component';


@NgModule({
  declarations: [ActiveinvestmentsComponent],
  imports: [
    CommonModule,
    ActiveinvestmentsRoutingModule
  ]
})
export class ActiveinvestmentsModule { }
