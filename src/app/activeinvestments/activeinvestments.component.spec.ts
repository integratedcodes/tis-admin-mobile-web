import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveinvestmentsComponent } from './activeinvestments.component';

describe('ActiveinvestmentsComponent', () => {
  let component: ActiveinvestmentsComponent;
  let fixture: ComponentFixture<ActiveinvestmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveinvestmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveinvestmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
