import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestmentmanagementRoutingModule } from './investmentmanagement-routing.module';
import { InvestmentmanagementComponent } from './investmentmanagement.component';


@NgModule({
  declarations: [InvestmentmanagementComponent],
  imports: [
    CommonModule,
    InvestmentmanagementRoutingModule
  ]
})
export class InvestmentmanagementModule { }
