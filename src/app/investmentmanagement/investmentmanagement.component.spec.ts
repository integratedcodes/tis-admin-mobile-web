import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestmentmanagementComponent } from './investmentmanagement.component';

describe('InvestmentmanagementComponent', () => {
  let component: InvestmentmanagementComponent;
  let fixture: ComponentFixture<InvestmentmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestmentmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestmentmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
