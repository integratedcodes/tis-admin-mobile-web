import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestmentlistdetailactiveComponent } from './investmentlistdetailactive.component';

describe('InvestmentlistdetailactiveComponent', () => {
  let component: InvestmentlistdetailactiveComponent;
  let fixture: ComponentFixture<InvestmentlistdetailactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestmentlistdetailactiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestmentlistdetailactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
