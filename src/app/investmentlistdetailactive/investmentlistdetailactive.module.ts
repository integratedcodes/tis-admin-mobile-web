import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvestmentlistdetailactiveComponent } from './investmentlistdetailactive.component';



@NgModule({
  declarations: [InvestmentlistdetailactiveComponent],
  imports: [
    CommonModule
  ]
})
export class InvestmentlistdetailactiveModule { }
