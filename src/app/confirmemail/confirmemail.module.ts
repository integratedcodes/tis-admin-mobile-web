import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmemailRoutingModule } from './confirmemail-routing.module';
import { ConfirmemailComponent } from './confirmemail.component';


@NgModule({
  declarations: [ConfirmemailComponent],
  imports: [
    CommonModule,
    ConfirmemailRoutingModule
  ]
})
export class ConfirmemailModule { }
