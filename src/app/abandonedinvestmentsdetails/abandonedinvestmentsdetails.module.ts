import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AbandonedinvestmentsdetailsRoutingModule } from './abandonedinvestmentsdetails-routing.module';
import { AbandonedinvestmentsdetailsComponent } from './abandonedinvestmentsdetails.component';


@NgModule({
  declarations: [AbandonedinvestmentsdetailsComponent],
  imports: [
    CommonModule,
    AbandonedinvestmentsdetailsRoutingModule
  ]
})
export class AbandonedinvestmentsdetailsModule { }
