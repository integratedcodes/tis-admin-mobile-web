import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbandonedinvestmentsdetailsComponent } from './abandonedinvestmentsdetails.component';

describe('AbandonedinvestmentsdetailsComponent', () => {
  let component: AbandonedinvestmentsdetailsComponent;
  let fixture: ComponentFixture<AbandonedinvestmentsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbandonedinvestmentsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbandonedinvestmentsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
