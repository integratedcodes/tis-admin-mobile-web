import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminuserprofileRoutingModule } from './adminuserprofile-routing.module';
import { AdminuserprofileComponent } from './adminuserprofile.component';


@NgModule({
  declarations: [AdminuserprofileComponent],
  imports: [
    CommonModule,
    AdminuserprofileRoutingModule
  ]
})
export class AdminuserprofileModule { }
