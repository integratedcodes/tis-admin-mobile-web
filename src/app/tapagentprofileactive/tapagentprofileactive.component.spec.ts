import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TapagentprofileactiveComponent } from './tapagentprofileactive.component';

describe('TapagentprofileactiveComponent', () => {
  let component: TapagentprofileactiveComponent;
  let fixture: ComponentFixture<TapagentprofileactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TapagentprofileactiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TapagentprofileactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
