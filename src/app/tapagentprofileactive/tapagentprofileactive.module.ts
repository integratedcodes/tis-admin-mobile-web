import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TapagentprofileactiveRoutingModule } from './tapagentprofileactive-routing.module';
import { TapagentprofileactiveComponent } from './tapagentprofileactive.component';


@NgModule({
  declarations: [TapagentprofileactiveComponent],
  imports: [
    CommonModule,
    TapagentprofileactiveRoutingModule
  ]
})
export class TapagentprofileactiveModule { }
