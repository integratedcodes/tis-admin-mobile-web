import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OngoinginvestmentsdetailsComponent } from './ongoinginvestmentsdetails.component';

describe('OngoinginvestmentsdetailsComponent', () => {
  let component: OngoinginvestmentsdetailsComponent;
  let fixture: ComponentFixture<OngoinginvestmentsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OngoinginvestmentsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OngoinginvestmentsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
