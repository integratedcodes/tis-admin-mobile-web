import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditprofilebasicinfoRoutingModule } from './editprofilebasicinfo-routing.module';
import { EditprofilebasicinfoComponent } from './editprofilebasicinfo.component';


@NgModule({
  declarations: [EditprofilebasicinfoComponent],
  imports: [
    CommonModule,
    EditprofilebasicinfoRoutingModule
  ]
})
export class EditprofilebasicinfoModule { }
