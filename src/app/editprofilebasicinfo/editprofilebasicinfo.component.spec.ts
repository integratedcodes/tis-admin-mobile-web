import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditprofilebasicinfoComponent } from './editprofilebasicinfo.component';

describe('EditprofilebasicinfoComponent', () => {
  let component: EditprofilebasicinfoComponent;
  let fixture: ComponentFixture<EditprofilebasicinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditprofilebasicinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditprofilebasicinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
