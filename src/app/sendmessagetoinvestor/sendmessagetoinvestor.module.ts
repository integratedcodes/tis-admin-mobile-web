import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SendmessagetoinvestorRoutingModule } from './sendmessagetoinvestor-routing.module';
import { SendmessagetoinvestorComponent } from './sendmessagetoinvestor.component';


@NgModule({
  declarations: [SendmessagetoinvestorComponent],
  imports: [
    CommonModule,
    SendmessagetoinvestorRoutingModule
  ]
})
export class SendmessagetoinvestorModule { }
