import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendmessagetoinvestorComponent } from './sendmessagetoinvestor.component';

describe('SendmessagetoinvestorComponent', () => {
  let component: SendmessagetoinvestorComponent;
  let fixture: ComponentFixture<SendmessagetoinvestorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendmessagetoinvestorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendmessagetoinvestorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
