import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnapprovedinvestmentsdetailsComponent } from './unapprovedinvestmentsdetails.component';

describe('UnapprovedinvestmentsdetailsComponent', () => {
  let component: UnapprovedinvestmentsdetailsComponent;
  let fixture: ComponentFixture<UnapprovedinvestmentsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnapprovedinvestmentsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnapprovedinvestmentsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
