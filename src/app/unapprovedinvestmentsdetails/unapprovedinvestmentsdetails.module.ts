import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnapprovedinvestmentsdetailsComponent } from './unapprovedinvestmentsdetails.component';



@NgModule({
  declarations: [UnapprovedinvestmentsdetailsComponent],
  imports: [
    CommonModule
  ]
})
export class UnapprovedinvestmentsdetailsModule { }
