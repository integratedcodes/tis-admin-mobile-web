import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidemenubarComponent } from './sidemenubar.component';



@NgModule({
  declarations: [SidemenubarComponent],
  imports: [
    CommonModule
  ]
})
export class SidemenubarModule { }
