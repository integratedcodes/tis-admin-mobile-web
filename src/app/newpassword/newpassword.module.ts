import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewpasswordRoutingModule } from './newpassword-routing.module';
import { NewpasswordComponent } from './newpassword.component';


@NgModule({
  declarations: [NewpasswordComponent],
  imports: [
    CommonModule,
    NewpasswordRoutingModule
  ]
})
export class NewpasswordModule { }
