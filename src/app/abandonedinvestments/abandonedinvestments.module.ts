import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AbandonedinvestmentsRoutingModule } from './abandonedinvestments-routing.module';
import { AbandonedinvestmentsComponent } from './abandonedinvestments.component';


@NgModule({
  declarations: [AbandonedinvestmentsComponent],
  imports: [
    CommonModule,
    AbandonedinvestmentsRoutingModule
  ]
})
export class AbandonedinvestmentsModule { }
