import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbandonedinvestmentsComponent } from './abandonedinvestments.component';

describe('AbandonedinvestmentsComponent', () => {
  let component: AbandonedinvestmentsComponent;
  let fixture: ComponentFixture<AbandonedinvestmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbandonedinvestmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbandonedinvestmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
